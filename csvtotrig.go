package main

import (
	"compress/gzip"
	"encoding/csv"
	"io"
	"os"

	log "github.com/sirupsen/logrus"
)

func convertCSV(path, target string) {

	// check if file exists
	if _, err := os.Stat(path); os.IsNotExist(err) {
		log.Infof("csv file %s does not exist", path)
	}

	file, err := os.Open(path)

	if err != nil {
		log.Errorf("csv file %s could not be found", path)
	}

	defer file.Close()

	// load stuff.
	// csvLines, err := csv.NewReader(file).ReadAll()

	// wg
	output := make(chan (ItemIn))
	go importCSV(file, output)
	writeTrig(output, target)
}

func importCSV(file *os.File, out chan (ItemIn)) {

	reader := csv.NewReader(file)
	reader.Comma = ';'

	rows := 0
	failed := 0

	for {
		record, err := reader.Read()
		rows += 1
		if rows == 1 {
			// skip header
			continue
		}

		if err != nil {
			if err == io.EOF {
				break
			}
			failed++
			continue
		}

		newI := ItemFromCSV(record)
		out <- newI

		if rows%100000 == 0 {
			log.Infof("rows done: %d", rows)
		}
	}

	close(out)
	log.Infof("parsed %d csv rows", rows)
}

func writeTrig(items chan (ItemIn), targetfile string) {
	// Define a template.
	const header = `
prefix dego: <https://data.labs.kadaster.nl/dego/energie/def/>
prefix id: <https://data.labs.kadaster.nl/dego/energie/id/>
prefix place: <https://data.labs.kadaster.nl/kadaster/kg/id/place/>
prefix xsd: <http://www.w3.org/2001/XMLSchema#>
	`
	fi, err := os.OpenFile(targetfile, os.O_WRONLY|os.O_CREATE, 0660)

	defer fi.Close()

	if err != nil {
		log.Printf("Error in Create\n")
		panic(err)
	}

	zw := gzip.NewWriter(fi)

	zw.Name = "dego.nl.trig"
	zw.Comment = "DEGO trig export"

	zw.Write([]byte(header))

	for item := range items {
		item.toTrigPlace(zw)
	}

	if err := zw.Close(); err != nil {
		log.Fatal(err)
	}
}

func main() {
	convertCSV("aalsmeer.csv", "aalsmeer.trig.gz")
}
