package main

import (
	"io"
	"log"
	"text/template"
)

type ItemIn struct {
	Pid                     string `json:"pid"`
	Vid                     string `json:"vid"`
	Numid                   string `json:"numid"`
	Straat                  string `json:"straat"`
	Postcode                string `json:"postcode"`
	Huisnummer              string `json:"huisnummer"`
	Huisletter              string `json:"huisletter"`
	Huisnummertoevoeging    string `json:"huisnummertoevoeging"`
	Oppervlakte             string `json:"oppervlakte"`
	Woningequivalent        string `json:"woningequivalent"`
	WoningType              string `json:"woning_type"`
	LabelscoreVoorlopig     string `json:"labelscore_voorlopig"`
	LabelscoreDefinitief    string `json:"labelscore_definitief"`
	Energieklasse           string `json:"energieklasse"`
	Gemeentecode            string `json:"gemeentecode"`
	Gemeentenaam            string `json:"gemeentenaam"`
	Buurtcode               string `json:"buurtcode"`
	Buurtnaam               string `json:"buurtnaam"`
	Wijkcode                string `json:"wijkcode"`
	Wijknaam                string `json:"wijknaam"`
	Provinciecode           string `json:"provinciecode"`
	Provincienaam           string `json:"provincienaam"`
	Point                   string `json:"point"`
	PandGasEanAansluitingen string `json:"pand_gas_ean_aansluitingen"`
	GroupId2020             string `json:"group_id_2020"`
	P6GasAansluitingen2020  string `json:"p6_gas_aansluitingen_2020"`
	P6Gasm32020             string `json:"p6_gasm3_2020"`
	P6Kwh2020               string `json:"p6_kwh_2020"`
	P6TotaalPandoppervlakM2 string `json:"p6_totaal_pandoppervlak_m2"`
	PandBouwjaar            string `json:"pand_bouwjaar"`
	PandGasAansluitingen    string `json:"pand_gas_aansluitingen"`
	Gebruiksdoelen          string `json:"gebruiksdoelen"`
}

func (i *ItemIn) toTrigPlace(output io.Writer) {

	if i.Vid == "" {
		return
	}

	const place = `
place:{{.Vid}}
  dego:energiegegevensPerPostcodegebied id:{{.GroupId2020}} ;
  dego:pandID "{{.Pid}}" ;
  dego:woningequivalent "{{.Woningequivalent}}"^^xsd:nonNegativeInteger ;
  dego:labelscore_voorlopig {{.LabelscoreVoorlopig}} ;
  dego:labelscore_definitief {{.LabelscoreDefinitief}} ;
  dego:energieklasse "{{.Energieklasse}}" ;
  dego:pandgasaansluitingen "{{.PandGasEanAansluitingen}}"^^xsd:nonNegativeInteger .
id:{{.GroupId2020}}
  dego:p6_gas_aansluitingen_2020 "{{.P6GasAansluitingen2020}}"^^xsd:nonNegativeInteger ;
  dego:p6_gasm3_2020 "{{.P6Gasm32020}}"^^xsd:nonNegativeInteger ;
  dego:p6_kwh_2020 "{{.P6Kwh2020}}"^^xsd:integer ;
  dego:p6_totaal_pandoppervlak_m2 "{{.P6TotaalPandoppervlakM2}}"^^xsd:nonNegativeInteger .
`
	t := template.Must(template.New("place").Parse(place))
	err := t.Execute(output, i)
	if err != nil {
		log.Println("executing template:", err)
	}
}

func ItemFromCSV(r []string) ItemIn {
	return ItemIn{
		Pid:                     r[0],
		Vid:                     r[1],
		Numid:                   r[2],
		Straat:                  r[3],
		Postcode:                r[4],
		Huisnummer:              r[5],
		Huisletter:              r[6],
		Huisnummertoevoeging:    r[7],
		Oppervlakte:             r[8],
		Woningequivalent:        r[9],
		WoningType:              r[10],
		LabelscoreVoorlopig:     r[11],
		LabelscoreDefinitief:    r[12],
		Energieklasse:           r[13],
		Gemeentecode:            r[14],
		Gemeentenaam:            r[15],
		Buurtcode:               r[16],
		Buurtnaam:               r[17],
		Wijkcode:                r[18],
		Wijknaam:                r[19],
		Provinciecode:           r[20],
		Provincienaam:           r[21],
		Point:                   r[22],
		PandGasEanAansluitingen: r[23],
		GroupId2020:             r[24],
		P6GasAansluitingen2020:  r[25],
		P6Gasm32020:             r[26],
		P6Kwh2020:               r[27],
		P6TotaalPandoppervlakM2: r[28],
		PandBouwjaar:            r[29],
		PandGasAansluitingen:    r[30],
		Gebruiksdoelen:          r[31],
	}
}
